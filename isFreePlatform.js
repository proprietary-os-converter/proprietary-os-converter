/** This script is because too many user with proprietary platform join my site. Therefore here it comes the idea
to send a message specific to those users in order to make them aware of the existance of free* alternatives that
involve Free-Software.
                             * free as in Freedom!

    Version=0.1
    Licence=GPL3 and following
**/

/** Set main variables and content **/
/** OSconverterLevel: 0 = do nothing
                      1 = show warning only on the top of the page
                      2 = show full page warning
**/
var OSconverterLevel=1;
/** showLink:     0 = don't show skip add link
                  1 = show skip add link
    WARNING: if OSconverterLevelis set to 2 and showLink is set to 0 the user won't
    be able to access the site content **/
var showLink=1;
// Message to show
function setMessage(whichLayer) {
  var txtHEAD='<div style="padding: 30px; background-color: #ffffbb; font-family: arial; font-size: 15px; font-weight: normal; color: #111111; line-height: 17px;">'
  var txtIT='<div style="width: 800px; margin: 0 auto 0 auto;"><p>Questo sito è condiviso con chi ha <strong>la passione per il Free-Software</strong>. Al momento tu non risulti tra detti appassionati siccome stai utilizzando un sistema operativo proprietario.<br />In ogni caso stai tranquillo, sei ancora in tempo per rimediare!</p><p>Come? Scaricati <a href="http://www.debian.org">Debian</a>, la mia distribuzione GNU/Linux preferita, e liberati dalle catene!<br /> Adotta anche tu il libero Software per una libera conoscenza.</p><p>Ecco che cosa è <a href="http://it.wikipedia.org/wiki/Software_libero">il Free-Software</a>.<br />Non ti piace Debian? Tranquillo che vi è <a href="http://distrowatch.com/">molta scelta</a> là fuori!</p></div>';
  var txtEnd='</div>';
  var StopAdLinkIT='<a href="javascript:notFreeButContinueWithoutFS();">Lo so che sono uno stupido a non passare subito al Free Software ma sono talmente cocciuto che per ora continuo così.</a><br />';
  var StopAdLinkEN='<a href="javascript:notFreeButContinueWithoutFS();">I know that I&#39;m really stupid in not using the Free-Software but I&#39;m pigheaded and I&#39;m continue without it.</a> <br />';
  var message = document.getElementById(whichLayer);
  if (showLink==0) {
    message.innerHTML=txtIT;}
  else if (showLink==1) {
    message.innerHTML=txtIT + StopAdLinkIT + StopAdLinkEN;}
}

// Create message div
function insertMessage() {
  var message=document.createElement("div");
  message.id="notFree_ByeBye";
  document.body.insertBefore(message, document.body.firstChild);
  setMessage("notFree_ByeBye");
}

// Test is the OS is free from the UserAgent data
function isFree_test() {
  var ua = navigator.userAgent.toLowerCase();
  var pa = navigator.platform.toLowerCase();
  return (
            (  (ua.indexOf('window') != -1) || (ua.indexOf('mac os x') != -1) || (ua.indexOf('iphone') != -1) || (ua.indexOf('blackberry') != -1) || 
               (ua.indexOf('webos') != -1) || (pa.indexOf('win') != -1) || (pa.indexOf('mac') !=-1) || (pa.indexOf('iphone') !=-1)
            ) && (location.href.indexOf('seenFSPage') == -1));
}

function notFree_showOnlyLayer(whichLayer) {
  var style2 = document.getElementById(whichLayer);
  var body = document.getElementsByTagName('body');
  body[0].innerHTML = style2.innerHTML;
}

/** MAIN CALL
Hides or shows the page whether the user-agent says that it is a free platform **/
function isFreePlatform_hideAndShow() {
  if (isFree_test()) {
    switch (OSconverterLevel)
    {
      case 0:
        //pass
        break;
      case 1:
        insertMessage();
        break;
      case 2:
        insertMessage();
        notFree_showOnlyLayer("notFree_ByeBye");
        break;
    }
  }
}

function notFreeButContinueWithoutFS() {
    if (location.href.indexOf('?') != -1)
        location.href += '&seenFSPage=1';
    else
        location.href += '?seenFSPage=1';
}

